from microbit import *
import random
import radio

version = '1.2'
badgenum = "000"
radio_on = True
standard_emojis = [ Image.HAPPY, Image.SAD, Image.ANGRY, Image.COW, Image.CONFUSED, Image.XMAS, Image.GHOST, Image.PACMAN, Image.DIAMOND, Image.STICKFIGURE ]
cur_emoji = 0
ped_count = 0
eightG_count = 0
freefall_count = 0
max_temp = 0
min_temp = 100
beacon_count = 0
last_infection = 0
emoji_display_on = False

def fast_scroll(msg):
    display.scroll(msg, delay=100, loop=False)

def wait_for_life(wake_on_movement):
    moved = False
    while not button_a.was_pressed() and not button_b.was_pressed() and not moved:
        if wake_on_movement and accelerometer.get_y() > -1000:
            moved = True
        sleep(1000)

def go_to_sleep():
    display.show(Image.ASLEEP)
    sleep(1000)
    radio.off()
    display.off()

def wake_up():
    radio.on()
    display.on()
    display.show(Image.HAPPY)
    sleep(1000)
    display.clear()

def check_for_sleep():
    sleep_counter = 0
    while accelerometer.get_y() < -1000:
        if sleep_counter == 2:
            display.show(Image.ASLEEP)
        sleep_counter += 1
        sleep(1000)
        if sleep_counter > 4:
          go_to_sleep()
          wait_for_life(wake_on_movement=True)
          wake_up()

def emoji_choice():
    global cur_emoji, emoji_display_on
    cur_time = running_time()
    emoji_display_on = True
    last_emoji = cur_emoji
    while (cur_time > running_time()-5000):
        display.show(standard_emojis[cur_emoji])
        if button_a.was_pressed():
            cur_emoji += 1
            if cur_emoji > len(standard_emojis)-1:
                cur_emoji = 0
            cur_time = running_time()
        if button_b.was_pressed():
            if last_emoji != cur_emoji:
                display.show(Image.YES)
                sleep(200)
                display.show(standard_emojis[cur_emoji])
            return
        sleep(250)

def save_settings():
    F = open('settings.txt', 'w')
    F.write(badgenum + '\n' + str(cur_emoji) + '\n' + str(ped_count) + '\n' + str(eightG_count) + '\n' + str(freefall_count) + '\n' + str(max_temp) + '\n' + str(min_temp) + '\n' + str(beacon_count) + '\n')
    F.close()

def infect(patient_zero):
    global last_infection
    cur_time = running_time()
    if (cur_time - last_infection > 900000): # no infections within 15 mins of each other
        last_infection = cur_time
        for i in range(60):  # infectious for roughly 5 minutes (sleep 5 seconds each loop)
            display.show(Image.SKULL)
            radio.send(patient_zero + " infect")
            sleep(5000)
            if patient_zero == badgenum:
                display.scroll("Patient Zero")
            else:
                display.scroll("Infected")
    else:
        display.scroll("Immune to " + patient_zero)

def check_radio():
    global beacon_count
    msg = radio.receive()
    if (msg):
        line = msg.rsplit() # line is now a list of strings
        if (len(line)>1):
            if line[1] == "beacon":  # someone knocks
                beacon_count += 1
                display.show(Image.TARGET)
                sleep(500)
            elif line[1] == "infect":  # we've been infected
                infect("999")
            elif len(line)>2:
                if line[1] == "hunt":  # scavenger hunt directions
                    for i in range(3,len(line)+1):
                        display.scroll(line[i-1])
            display.clear()
    if random.randrange(6000) == 0:  # beacon every 5 minutes
        radio.send(badgenum + " beacon")
        save_settings()
        display.show(standard_emojis[cur_emoji])
        sleep(200)
        if not emoji_display_on:
            display.clear()
        if random.randrange(144) == 0: # zombie twice a day
            infect(badgenum)

def check_buttons():
      global emoji_display_on
      if button_a.was_pressed():
          sleep(100)
      if button_b.was_pressed():
          if emoji_display_on:
              display.clear()
              emoji_display_on = False
          else:
              emoji_choice()

def check_motion():
    global ped_count, eightG_count, freefall_count, max_temp, min_temp
    gestures = accelerometer.get_gestures()
    for i in range(len(gestures)):
        if gestures[i]=="freefall":
            freefall_count += 1
        elif gestures[i]=="shake":
            display.show(Image.SUPRISED)
            sleep(100)
            display.clear()
            ped_count += 1
        elif gestures[i]=="8g":
            display.show(Image.RABBIT)
            sleep(100)
            display.clear()
            eightG_count += 1
    temp = temperature()
    if temp > max_temp:
        max_temp = temp
    if temp < min_temp:
        min_temp = temp

def idle_loop():
    while True:
        check_motion()
        check_buttons()
        check_for_sleep()
        check_radio()
        sleep(50)

display.show(Image.HEART)
sleep(5000)
display.clear()
try:
    FI = open('settings.txt')
    badgenum = FI.readline().rstrip()
    cur_emoji = int(FI.readline().rstrip())
    ped_count = int(FI.readline().rstrip())
    eightG_count = int(FI.readline().rstrip())
    freefall_count = int(FI.readline().rstrip())
    max_temp = int(FI.readline().rstrip())
    min_temp = int(FI.readline().rstrip())
    beacon_count = int(FI.readline().rstrip())
except:
    sleep(200)
radio.on()
radio.config(power=3)
idle_loop()
