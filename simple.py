from microbit import *
import random
import radio

version = '0.9'
gitlab_emoji = Image("90009:"
                     "99099:"
                     "99999:"
                     "09990:"
                     "00900")
standard_emojis = [ gitlab_emoji, Image.HAPPY, Image.SAD, Image.ANGRY, Image.COW, Image.CONFUSED, Image.XMAS, Image.GHOST, Image.PACMAN, Image.DIAMOND, Image.STICKFIGURE ]

def wait_for_life(wake_on_movement):
    moved = False
    while not button_a.was_pressed() and not button_b.was_pressed() and not moved:
        if wake_on_movement and accelerometer.get_y() > -1000:
            moved = True
        sleep(1000)

def go_to_sleep():
    display.show(Image.ASLEEP)
    sleep(1000)
    radio.off()
    display.off()

def wake_up():
    radio.on()
    display.on()
    display.show(Image.HAPPY)
    sleep(1000)
    display.clear()

def check_for_sleep():
    sleep_counter = 0
    while accelerometer.get_y() < -1000:
        if sleep_counter == 2:
            display.show(Image.ASLEEP)
        sleep_counter += 1
        sleep(1000)
        if sleep_counter > 4:
          go_to_sleep()
          wait_for_life(wake_on_movement=True)
          wake_up()

def check_radio():
    msg = radio.receive()
    if (msg):
        msg1,msg2,msg3 = msg.split()
        if (len(msg2)>0):
            if msg2 == "hunt":  # scavenger hunt directions
                if (len(msg3)>0):
                    for i in range(5):
                        display.scroll(msg3)

def emoji_show():
    while not button_b.was_pressed():
        display.show(random.choice(standard_emojis)
        sleep(5000)

def check_buttons():
      if button_a.was_pressed():
          sleep(100)
      if button_b.was_pressed():
          emoji_show()

def check_for_input():
    check_buttons()
    check_for_sleep()
    check_radio()

def idle_loop():
    while True:
        check_for_input()
        #random_event()
        sleep(50)

display.show(gitlab_emoji)
sleep(5000)
display.clear()
radio.on()
radio.config(power=3)
idle_loop()
