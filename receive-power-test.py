from microbit import *
import random
import radio

def idle_loop():
    while True:
        msg = radio.receive()
        if (msg=='infect'):
            display.show(Image.SAD)
            sleep(5000)
            display.clear()
        sleep(500)

display.show(Image.YES)
sleep(1000)
display.clear()
radio.on()
idle_loop()
