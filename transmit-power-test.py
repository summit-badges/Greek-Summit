from microbit import *
import random
import radio

rpower = 3

def idle_loop():
    while True:
        if button_b.was_pressed():
            radio.send("000 hunt foo you two")
            display.scroll("Sent")
            sleep(1000)
        sleep(100)

display.show(Image.YES)
sleep(1000)
display.clear()
radio.on()
radio.config(power=rpower)
idle_loop()
